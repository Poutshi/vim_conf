
"Plug 'vim-airline/vim-airline'            " Lean & mean status/tabline for vim
"Plug 'vim-airline/vim-airline-themes'     " Themes for airline
" Plug 'fisadev/FixedTaskList.vim'          " Pending tasks list
" Plug 'yuttie/comfortable-motion.vim'      " Smooth scrolling
" Plug 'MattesGroeger/vim-bookmarks'        " Bookmarks
" Plug 'thaerkh/vim-indentguides'           " Visual representation of indents
" Plug 'neomake/neomake'                    " Asynchronous Linting and Make Framework
" Plug 'Shougo/deoplete.nvim'               " Asynchronous Completion
" Plug 'roxma/nvim-yarp'                    " Deoplete Dependency #1
" Plug 'roxma/vim-hug-neovim-rpc'           " Deoplete Dependency #2
" Plug 'Valloric/YouCompleteMe'             " automcomplete c-lang family
"-------------------=== Language Server Protocol ===-------------
" Plug 'neoclide/coc-python', {'do': 'yarn install --frozen-lockfile'}
" "-------------------=== Buffers / Windows===-------------------
"Plug 'vim-ctrlspace/vim-ctrlspace'        " Tabs/Buffers/Fuzzy/Workspaces/Bookmarks
" "-------------------=== Snippets support ===--------------------
" Plug 'garbas/vim-snipmate'                " Snippets manager
" Plug 'MarcWeber/vim-addon-mw-utils'       " dependencies #1
" Plug 'tomtom/tlib_vim'                    " dependencies #2
" Plug 'honza/vim-snippets'                 " snippets repo
" 
" "-------------------=== Languages support ===-------------------
" Plug 'scrooloose/nerdcommenter'           " Easy code documentation
" Plug 'mitsuhiko/vim-sparkup'              " Sparkup(XML/jinja/htlm-django/etc.) support
" Plug 'w0rp/ale'
" 
" "-------------------=== Python  ===-----------------------------
" Plug 'klen/python-mode'                   " Python mode (docs, refactor, lints...)
" Plug 'hynek/vim-python-pep8-indent'
" Plug 'mitsuhiko/vim-python-combined'
" Plug 'mitsuhiko/vim-jinja'
" Plug 'jmcantrell/vim-virtualenv'
