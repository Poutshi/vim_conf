
## Firt steps 

### If using Vundle

mkdir ~/.vim/bundle

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

### If using Vim-Plug

curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

## File tree

.
├── autoload
├── bundle
├── coc_vimrc
├── colors
├── Passwords.kdbx
├── README.md
├── terminator
├── typical_learning_curves.png
├── vim_future
├── vim_future.drawio
├── vim_future.png
├── vim_learnig_curve.drawio
├── vim_learnig_curve.png
├── vimrc
├── vimrc.bak -> /home/akara/.vim/vimrc
├── vimrc_basic
├── vimrc_full
├── vimrc_python
└── vimrc_vundle


vimrc_plug: A version with vim-plug as plugin manager
vimrc_vundle: A version of vimrc file with Vundle as plugin manager

## Install extension

:CocInstall coc-python

## TODO

Extract the plugin install part from the global vimrc file so I can switch between plugin managers easily.

-------------------------------------

# ZSH

## Install zsh

```bash
sudo apt install zsh
```

## Make it your default shell

```bash
chsh -s $(which zsh)
```

## Install Oh-My-Zsh

#### via curl

```shell
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

#### via wget

```shell
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

----------------------------------------------------
        Search 
----------------------------------------------------

## Grep 

``bash 
curl -LO https://github.com/BurntSushi/ripgrep/releases/download/11.0.2/ripgrep_11.0.2_amd64.deb
sudo dpkg -i ripgrep_11.0.2_amd64.deb
```
## FZF

```bash
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install
source ~/.zshrc
```



-------------------------------------

## Poweline
### Poweline fonts 

```bash
sudo apt-get install fonts-powerline
sudo -H pip3 install --user git+git://github.com/powerline/powerline
```

------------------------------------

# File mangers

## Ranger 

```bash
sudo apt install ranger 
```

