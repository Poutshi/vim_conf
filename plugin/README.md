[from vimrc to vim folder](https://vimways.org/2018/from-vimrc-to-vim/)

Don't use the source command for non mandatory plugins as source will raise an error if the plugin is not found. Vim will still work but the error is annoying and errors are never good news. 

For instance the file `coc_nvim/coc.vim` is here to replace the former source in the vimrc.
```bash
"==========================================================" Coc.nvim configs
" source ~/.vim/coc_vimrc
```

## What’s a plugin, anyway?

Why should we put the script in `~/.vim/plugin?` You might object that our example isn’t a real plugin; it’s just a single function. However, that’s not a meaningful distinction to Vim. At startup, it sources any and all `.vim` files in the plugin subdirectory of each of the directories in `'runtimepath'`. It makes no difference what those files actually contain.
