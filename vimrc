"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
"                                                                                                "
"                                          .::::.                                                "
"                             ___________ :;;;;:`____________                                    "
"                             \_________/ ?????L \__________/                                    "
"                               |.....| ????????> :.......'                                      "
"                               |:::::| $$$$$$"`.:::::::' ,                                      "
"                              ,|:::::| $$$$"`.:::::::' .OOS.                                    "
"                            ,7D|;;;;;| $$"`.;;;;;;;' .OOO888S.                                  "
"                          .GDDD|;;;;;| ?`.;;;;;;;' .OO8DDDDDNNS.                                "
"                           'DDO|IIIII| .7IIIII7' .DDDDDDDDNNNF`                                 "
"                             'D|IIIIII7IIIII7' .DDDDDDDDNNNF`                                   "
"                               |EEEEEEEEEE7' .DDDDDDDNNNNF`                                     "
"                               |EEEEEEEEZ' .DDDDDDDDNNNF`                                       "
"                               |888888Z' .DDDDDDDDNNNF`                                         "
"                               |8888Z' ,DDDDDDDNNNNF`                                           "
"                               |88Z'    "DNNNNNNN"                                              "
"                               '"'        "MMMM"                                                "
"                                            ""                                                  "
"                                                                                                "
"      ___    ____                                            __   _         _    ________  ___  "
"     /   |  / / /  __  ______  __  __   ____  ___  ___  ____/ /  (_)____   | |  / /  _/  |/  /  "
"    / /| | / / /  / / / / __ \/ / / /  / __ \/ _ \/ _ \/ __  /  / / ___/   | | / // // /|_/ /   "
"   / ___ |/ / /  / /_/ / /_/ / /_/ /  / / / /  __/  __/ /_/ /  / (__  )    | |/ // // /  / /    "
"  /_/  |_/_/_/   \__, /\____/\__,_/  /_/ /_/\___/\___/\__,_/  /_/____/     |___/___/_/  /_/     "
"                   /_/                                                                          "
"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

set nocompatible              " required
set hidden
set showtabline=0

let mapleader = " "
let maplocalleader = " "

call plug#begin('~/.vim/bundle')

" Add all your plugins here (note older versions of Vundle used Bundle instead of Plugin)
"-------------------=== Code/Project navigation ===-------------
Plug 'scrooloose/nerdtree'                " Project and file navigation
Plug 'Xuyuanp/nerdtree-git-plugin'        " NerdTree git functionality
Plug 'majutsushi/tagbar'                  " Class/module browser
Plug 'mileszs/ack.vim'                    " Ag/Grep
Plug 'tpope/vim-commentary'
"-------------------=== Language Server Protocol ===-------------
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'derekwyatt/vim-scala'
Plug 'scalameta/coc-metals', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-tsserver', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-html', {'do': 'yarn install --frozen-lockfile'}

" "-------------------=== Colors  ===-------------------------------
" Plug 'flazz/vim-colorschemes'             " Colorschemes
" "-------------------=== Other ===-------------------------------
Plug 'tpope/vim-surround'                 " Parentheses, brackets, quotes, XML tags, and more
" Plug 'vimwiki/vimwiki'                    " Personal Wiki
Plug 'jreybert/vimagit'                   " Git Operations
" Plug 'kien/rainbow_parentheses.vim'       " Rainbow Parentheses
" Plug 'chriskempson/base16-vim'            " Base 16 colors
" Plug 'ryanoasis/vim-devicons'             " Dev Icons
Plug 'jpalardy/vim-slime'                   " send text to a terminal
Plug 'skywind3000/asyncrun.vim'             " Launch shell commands asynchronously
" 
" "-------------------=== Buffers / Windows===-------------------
Plug 'junegunn/goyo.vim'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" "-------------------=== Python  ===-----------------------------
Plug 'jupyter-vim/jupyter-vim'

" "-------------------=== Markdown  ===-----------------------------
Plug 'junegunn/vim-easy-align'

" "-------------------=== Latex  ===-----------------------------
Plug 'lervag/vimtex'

" All of your Plugins must be added before the following line
call plug#end()               " required


" "-----------------=== RTP ===-------------------------------
set rtp+=/usr/local/lib/python3.6/dist-packages/powerline/bindings/vim

"=====================================================
"" General settings
"=====================================================
if filereadable(expand("~/.vimrc_background"))
  source ~/.vimrc_background
endif
set encoding=utf-8

" let g:airline_theme='base16_spacemacs'             " set airline theme
syntax enable                               " enable syntax highlighting

set pyxversion=0
let g:loaded_python_provider = 1
set shell=/bin/bash
set number                                  " show line numbers
set ruler
set ttyfast                                 " terminal acceleration

set tabstop=4                               " 4 whitespaces for tabs visual presentation
set shiftwidth=4                            " shift lines by 4 spaces
set smarttab                                " set tabs for a shifttabs logic
set expandtab                               " expand tabs into spaces
set autoindent                              " indent when moving to the next line while writing code

" set cursorline                              " shows line under the cursor's line
set showmatch                               " shows matching part of bracket pairs (), [], {}

set enc=utf-8	                            " utf-8 by default

set nobackup 	                            " no backup files
set nowritebackup                           " only in case you don't want a backup file while editing
set noswapfile 	                            " no swap files

set backspace=indent,eol,start              " backspace removes all (indents, EOLs, start) What is start?

set scrolloff=20                            " let 10 lines before/after cursor during scroll

set clipboard=unnamed                       " use system clipboard

set exrc                                    " enable usage of additional .vimrc files from working directory
set secure                                  " prohibit .vimrc files to execute shell, create files, etc...

set linebreak                               " split the long lines at a space carachter. 
set foldmethod=syntax
set nofoldenable

" Colorschemes
" ============
set background=dark
colorscheme wombat256mod
let base16colorspace=256
set t_Co=256                                " 256 colors
"set guifont=mononoki\ Nerd\ Font\ 18
" colorscheme base16-default-dark             " set vim colorscheme

" Search settings
"============================================
set incsearch	                            " incremental search
set hlsearch	                            " highlight search results

"============================================ Tabs / Buffers settings
tab sball
set switchbuf=useopen
set laststatus=2
nmap <F9> :bprev<CR>
nmap <F10> :bnext<CR>
nmap <silent> <leader>q :SyntasticCheck # <CR> :bp <BAR> bd #<CR>
nnoremap <leader>gf :Goyo<CR>

"==========================================================" Search / Find files
set wildmenu
set wildmode=list:full
set path+=**
set wildignorecase
set ignorecase
set smartcase

"==========================================================" Global remapping
inoremap jk <esc>

"==========================================================" Terminal configs
tnoremap <leader>tt <c-w>N

"=====================================================
"" Search Remap
"=====================================================
nnoremap <leader>f :find *
nnoremap <leader>r :%s///g<Left><Left>
nnoremap <leader>rc :%s///gc<Left><Left><Left>
xnoremap <leader>r :%s///g<Left><Left>
xnoremap <leader>rc :%s///gc<Left><Left><Left>

"=====================================================
"" Relative Numbering 
"=====================================================
nnoremap <F4> :set relativenumber!<CR>

"=====================================================
"" Comfortable Motion Settings
"=====================================================
"let g:comfortable_motion_scroll_down_key = "j"
"let g:comfortable_motion_scroll_up_key = "k"
"let g:comfortable_motion_no_default_key_mappings = 1
"let g:comfortable_motion_impulse_multiplier = 25  " Feel free to increase/decrease this value.
"nnoremap <silent> <C-d> :call comfortable_motion#flick(g:comfortable_motion_impulse_multiplier * winheight(0) * 2)<CR>
"nnoremap <silent> <C-u> :call comfortable_motion#flick(g:comfortable_motion_impulse_multiplier * winheight(0) * -2)<CR>
"nnoremap <silent> <C-f> :call comfortable_motion#flick(g:comfortable_motion_impulse_multiplier * winheight(0) * 4)<CR>
"nnoremap <silent> <C-b> :call comfortable_motion#flick(g:comfortable_motion_impulse_multiplier * winheight(0) * -4)<CR>

"=====================================================
"" Motion Settings
"=====================================================

set mouse=a

"=====================================================
"" AirLine settings
"=====================================================
"let g:airline#extensions#tabline#enabled=1
"let g:airline#extensions#tabline#formatter='unique_tail'
" let g:airline_powerline_fonts=1

"=====================================================
"" Poweline settings
"=====================================================
python3 from powerline.vim import setup as powerline_setup
python3 powerline_setup()
python3 del powerline_setup

"=====================================================
"" NERDTree settings
"=====================================================
let NERDTreeIgnore=['\.pyc$', '\.pyo$', '__pycache__$']     " Ignore files in NERDTree
let NERDTreeWinSize=40
autocmd VimEnter * if !argc() | NERDTree | endif  " Load NERDTree only if vim is run without arguments
nnoremap <F3> :NERDTreeToggleVCS %:p:h<CR>

"=====================================================
"" NERDComment Settings 
"=====================================================
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'

" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1

" Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }

" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1

" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1
 
"=====================================================" Indent Guides Settings 
set listchars=tab:›\ ,trail:•,extends:#,nbsp:.

"=====================================================
"" TagBar settings
"=====================================================
" let g:tagbar_autofocus=0
" let g:tagbar_width=42
" autocmd BufEnter *.py :call tagbar#autoopen(0)
" autocmd BufWinLeave *.py :TagbarClose
nmap <F8> :TagbarToggle<CR>

imap <F5> <Esc>:w<CR>:!clear;python %<CR>


"=====================================================
"" Arrow keys disable
"=====================================================
" no <down> <Nop>
" no <left> <Nop>
" no <right> <Nop>
" no <up> <Nop>
" 
" ino <down> <Nop>
" ino <left> <Nop>
" ino <right> <Nop>
" ino <up> <Nop>
" 
" vno <down> <Nop>
" vno <left> <Nop>
" vno <right> <Nop>
" vno <up> <Nop>


"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>


"=====================================================
"" YouCompleteMe
"=====================================================

"let g:ycm_autoclose_preview_window_after_completion=1
"map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>

"=====================================================
"" Markdown
"=====================================================

" Align GitHub-flavored Markdown tables
au FileType markdown vmap <Leader>t :EasyAlign*<Bar><Enter>

" Rmarkdown 

augroup rmarkdown
    au! BufRead,BufNewFile *.Rmd  set filetype=markdown
    au! BufRead,BufNewFile *.Rpres  set filetype=markdown
    au! BufRead,BufNewFile *.rmd  set filetype=markdown
"Trying to compile after each insert mode leave
"  au! FileAppendPost *.rmd :!echo<space>"require(rmarkdown);<space>render('<c-r>%')"<space>\|<space>R<space>--vanilla<enter>
augroup END
autocmd FileType markdown map <F5> :!echo<space>"require(rmarkdown);<space>render('<c-r>%')"<space>\|<space>R<space>--vanilla<enter>

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

nmap <space>li <plug>(vimtex-info)

"==========================================================" Vimtex configs
let g:tex_flavor = 'latex'

"=====================================================
"" Jupyter-vim
"=====================================================

" Run current file
nnoremap <buffer> <silent> <localleader>jr :JupyterRunFile<CR>
nnoremap <buffer> <silent> <localleader>ji :PythonImportThisFile<CR>

" Change to directory of current file
nnoremap <buffer> <silent> <localleader>jd :JupyterCd %:p:h<C>

" Send a selection of lines
nnoremap <buffer> <silent> <localleader>jc :JupyterSendCell<CR>
nnoremap <buffer> <silent> <localleader>jv :JupyterSendRange<CR>
nnoremap <buffer> <silent> <localleader>jl :JupyterSendCode<CR>
nmap     <buffer> <silent> <localleader>e <Plug>JupyterRunTextObj
vmap     <buffer> <silent> <localleader>e <Plug>JupyterRunVisual

nnoremap <buffer> <silent> <localleader>U :JupyterUpdateShell<CR>

" Debugging maps
nnoremap <buffer> <silent> <localleader>jb :PythonSetBreak<CR>

"==========================================================" Slime
let g:slime_target = "vimterminal"
let g:slime_python_ipython = 1
let g:slime_no_mappings = 1
xmap <leader>js   <Plug>SlimeRegionSend
" nmap <leader>js   <S-v><Plug>SlimeRegionSend
nmap <c-c>v       <Plug>SlimeConfig

"==========================================================" Scala configs
au BufRead,BufNewFile *.sbt set filetype=scala

"==========================================================" Json configs
autocmd FileType json syntax match Comment +\/\/.\+$+

"==========================================================" Fzf configs
nnoremap <C-p> :<C-u>FZF<CR>
nnoremap <leader>fb :<C-u>Buffers<CR>
nnoremap <leader>fgf :<C-u>GFiles<CR>

